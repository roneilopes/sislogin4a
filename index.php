<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Página Inicial</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <style>
            .centro{
                width: 50%;
                min-width: 200px;
                margin: 0 auto;
                float: none;
            }
        </style>
    </head>
    <body>
        <div class="row-fluid">
            <div class="centro">
                <h4>Bem-vindo ao Sistema SisLogin Teste</h4>
                <p class="text-info">Clique no botão abaixo para acessar o sistema</p>
                <p class="text-info">Dupla: Ronei e Charles</p>
                <a href="app/LoginCtrl/verificarLogin" class="btn btn-success">Fazer Login</a>
            </div>
        </div>
    </body>
</html>
