<?php
class UsuarioDao extends Dao{
    
    /**
     * @param Usuario $obj
     */
    public function atualizar($obj) {
        $sql = "UPDATE usuario SET nome=:nome, login=:login,"
                . "senha=:senha WHERE cod=:cod";
        try{
            $stmt = $this->c->prepare($sql);
            $stmt->bindValue(":nome", $obj->getNome());
            $stmt->bindValue(":login", $obj->getLogin());
            $stmt->bindValue(":senha", $obj->getSenha());
            $stmt->bindValue(":cod", $obj->getCod());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
        
    }

    public function excluir($cod) {
        $sql = "DELETE FROM usuario WHERE cod=:cod";
        try{
            $stmt = $this->c->prepare($sql);
            $stmt->bindParam(":cod", $cod, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $ex) {
            return 0;
        }
    }
    /**
     * @param Usuario $obj
     */
    public function salvar($obj) {
        $sql = "INSERT INTO usuario(cod,nome,login,senha)"
                . " VALUES(:cod,:nome,:login,:senha)";
        try{
            $stmt = $this->c->prepare($sql);
            $stmt->bindValue(":cod", $obj->getCod());
            $stmt->bindValue(":nome", $obj->getNome());
            $stmt->bindValue(":login", $obj->getLogin());
            $stmt->bindValue(":senha", $obj->getSenha());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $ex) {
            return 0;
        }
    }
    public function buscarTodos() {
        $sql ="SELECT * FROM usuario";
        try{
            $query = $this->c->prepare($sql);
            $query->execute();
            $query->setFetchMode(
                    PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
                    "Usuario");
            $rs = $query->fetchAll();
            return $rs;
        } catch (Exception $ex) {
            return null;
        }
    }
    public function buscarPorLoginSenha($login, $senha) {
        $sql ="SELECT * FROM usuario WHERE login=:login "
                . "and senha=:senha";
        try{
            $query = $this->c->prepare($sql);
            $query->bindParam(":login", $login);
            $query->bindParam(":senha", $senha);
            $query->execute();
            $query->setFetchMode(
                    PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
                    "Usuario");
            $rs = $query->fetchAll();
            return $rs[0];
        } catch (Exception $ex) {
            return null;
        }
    }
    
    public function buscarPorCod($cod) {
        $sql ="SELECT * FROM usuario WHERE cod=:cod ";
        try{
            $query = $this->c->prepare($sql);
            $query->bindParam(":cod", $cod);
            $query->execute();
            $query->setFetchMode(
                    PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
                    "Usuario");
            $rs = $query->fetchAll();
            return $rs[0];
        } catch (Exception $ex) {
            return null;
        }
    }
    
    public function buscarPorNome($nome) {
        $sql ="SELECT * FROM usuario WHERE nome LIKE '%$nome%' ";
        try{
            $query = $this->c->prepare($sql);
            //$query->bindParam(":nome", $nome);
            $query->execute();
            $query->setFetchMode(
                    PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
                    "Usuario");
            $rs = $query->fetchAll();
            return $rs;
        } catch (Exception $ex) {
            return null;
        }
    }
    
    public function buscarPorLogin($login) {
        $sql ="SELECT * FROM usuario WHERE login=:login ";
        try{
            $query = $this->c->prepare($sql);
            $query->bindParam(":login", $login);
            $query->execute();
            $query->setFetchMode(
                    PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
                    "Usuario");
            $rs = $query->fetchAll();
            return $rs[0];
        } catch (Exception $ex) {
            return null;
        }
    }
}
