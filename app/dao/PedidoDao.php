<?php

class PedidoDao extends Dao {

    /**
     * 
     * @param Pedido $obj
     */
    public function atualizar($obj) {
        $sql = "UPDATE pedido SET total = :total, cliente = :cliente, data = :data"
                . "     WHERE cod = :cod";
        try{
            $this->c->beginTransaction();
            $stmt = $this->c->prepare($sql);
            $stmt->bindParam(":cod", $obj->getcod());
            $stmt->bindParam(":total", $obj->getTotal());
            $stmt->bindParam(":cliente", $obj->getCliente());
            $stmt->bindParam(":data", $obj->getData());
            $stmt->execute();
            if($this->atualizarItens($obj)){
                $this->c->commit();
            }else{
                $this->c->rollBack();
            }
            return $stmt->rowCount();            
        } catch (Exception $exc) {
            $this->c->rollBack();
            throw new Exception($exc->getMessage());
        }
    }

    public function excluir($cod) {
        
    }

    /**
     * @param Pedido $obj
     */
    public function salvar($obj) {
        $sql = "INSERT INTO pedido(total,cliente,data) "
                . "VALUES (:total,:cliente,:data)";
        try {
            $this->c->beginTransaction();
            $comando = $this->c->prepare($sql);
            $comando->bindValue(":total", $obj->getTotal());
            $comando->bindValue(":cliente", $obj->getCliente());
            $comando->bindValue(":data", $obj->getData());
            $comando->execute();
            $obj->setCod($this->c->lastInsertId());
            if ($this->salvarItens($obj)) {
                $this->c->commit();
            } else {
                $this->c->rollBack();
            }
            return $comando->rowCount();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    /**
     * @param Pedido $obj
     */
    private function salvarItens($obj) {
        $itemDao = new ItemDao();
        foreach ($obj->getItens() as $item) {
            $item->setPedido($obj);
            $salvou = $itemDao->salvar($item);
            if($salvou !== 1){
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @param Pedido $obj
     */
    private function atualizarItens($obj) {
        $daoItem = new ItemDao();
        $itensAtual - $obj->getItens();
        $itensAntes - $daoItem->buscarPorCodPedido($obj->getCod());
        $this->removerItens($itensAntes, $itensAtual, $daoItem);
        foreach ($itensAtual as $i){
            $achou = $this->procurarItens($i, $itensAntes);
            $resultado = $achou ? $daoItem->atualizar($i) : $daoItem->salvar($i);
            if($resultado != 1){
                return false;
            }
        }
        return true;
    }
    
    private function removerItens($itensAntes, $itensAtual, $dao){
        foreach ($itensAntes as $i){
            $achou = $this->procurarItens($i, $itensAtual);
            if(!$achou){
                $dao->excluir($i->getNum(), $i->getProduto()->getCod(), $i->getPedido()->getCod());
            }
        }
    }

    private function procurarItens($itemProcurar, $listaItens) {
        $chaveP = $itemProcurar->getNum().
                  $itemProcurar->getProduto()->getCod().
                  $itemProcurar->getPedido()->getCod();
        $achou = 0;
        foreach ($listaItens as $ia){
            $chave = $ia->getNum().
                     $ia->getProduto()->getCod().
                     $ia->getPedido()->getCod();
            if($chave == $chaveP){
                $achou = 1;
            }
        }
        return $achou === 1;
    }
    
    public function buscarPorCod($cod) {
        $sql = "SELECT * FROM pedido WHERE cod = :cod";
        try {
            $query = $this->c->prepare($sql);
            $query->bindParam(":cod", $cod, PDO::PARAM_INT);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Pedido");
            $rs = $query->fetchAll();
            if(count($rs)==1){
                $this->insereItens($rs[0]);
                return $rs[0];
            }
            return null;
        } catch (Exception $ex) {
            return null;
        }
        
    }

    public function insereItens($pedido) {
        $iDao =  new ItemDAO();
        $itens = $iDao->buscarPorCodPedido($pedido->getCod());
        $pedido->setItens($itens);
    }

    public function buscarTodos($quant, $inicio=0) {
        $sql = "SELECT * FROM pedido limit :inicio, :quant";
        try {
            $query = $this->c->prepare($sql);
            $query->bindValue(":inicio", $inicio);
            $query->bindValue(":quant", $quant);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Pedido");
            $rs = $query->fetchAll();
            foreach ($rs as $ped){
                $this->insereItens($ped);
            }
            return $rs;
        } catch (Exception $ex) {
            return 0;
        }
    }
}
