<?php
class ProdutoDao extends Dao{

    /**
     * @param Produto $pr
     */
    public function salvar($pr){
        try {
            $sql = "INSERT INTO produto(descricao,valor,marca) VALUES (:desc,:valor,:marca)";
            $stmt = $this->c->prepare($sql);
            $stmt->bindValue(":desc", $pr->getDescricao());
            $stmt->bindValue(":valor", $pr->getValor());
            $stmt->bindValue(":marca", $pr->getMarca());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            return 0;
        }
    }
    /**
     * @param Produto $pr
     */
    public function atualizar($pr){
        $sql = "UPDATE produto SET descricao=:desc, valor=:valor, "
                . "marca=:marca WHERE cod=:cod";
        try {
            $stmt = $this->c->prepare($sql);
            $stmt->bindParam(":cod", $pr->getCod());
            $stmt->bindParam(":desc", $pr->getDescricao());
            $stmt->bindParam(":valor", $pr->getValor());
            $stmt->bindParam(":marca", $pr->getMarca());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $exc) {
            return 0;
        }
    }
    
    public function excluir($cod){
        $sql = "DELETE FROM produto WHERE cod=:cod";
        try{
            $stmt = $this->c->prepare($sql);
            $stmt->bindParam(":cod", $cod);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $ex) {
            return 0;
        }
    }
    
    public function buscarPorCod($cod){
        $sql = "SELECT * FROM produto WHERE cod=:cod ";
        try{
            $query = $this->c->prepare($sql);
            $query->bindParam(":cod", $cod);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,"Produto");
            $rs = $query->fetchAll();
            return $rs[0];
        } catch (Exception $ex) {
            return 0;
        }
    }
    
    public function buscarPorDescricao($descricao){
        $sql = "SELECT * FROM produto WHERE descricao LIKE '%$descricao%'";
        try{
            $query = $this->c->prepare($sql);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,"Produto");
            $rs = $query->fetchAll();
            return $rs;
        } catch (Exception $ex) {
            return 0;
        }
    }
    
    public function buscarTodos(){
        $sql = "SELECT * FROM produto";        
        try{
            $query = $this->c->prepare($sql);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,"Produto");
            $rs = $query->fetchAll();
            return $rs;
        } catch (Exception $ex) {
            return 0;
        }
    }

}
