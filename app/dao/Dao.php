<?php
abstract class Dao {
    /**
     * @var PDO 
     */
    protected $c;
    
    public function __construct() {
        $this->c = Conexao::getConn();
    }
    
    //evita que a classe seja clonada
    public function __clone() {
    }
    
    public function __destruct() {
        foreach ($this as $key => $value){
            unset($this->$key);
        }
    }
    public abstract function salvar($obj);
    public abstract function atualizar($obj);
    //public abstract function excluir($cod);
}
