<?php

class ItemDao extends Dao {

    /**
     * @param Item $item
     */
    public function salvar($item) {
        try {
            $sql = "INSERT INTO item(num,quant,codProduto,codPedido)"
                    . " VALUES (:num,:quant,:codProd,:codPedido)";
            $stmt = $this->c->prepare($sql);
            $stmt->bindValue(":num", $item->getNum());
            $stmt->bindValue(":quant", $item->getQuant());
            $stmt->bindValue(":codProd", $item->getProduto()->getCod());
            $stmt->bindValue(":codPedido", $item->getPedido()->getCod());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $exc) {
            throw new \Exception($exc->getMessage());
        }
    }

    /**
     * @param Item $item
     */
    public function atualizar($item) {
        $sql = "UPDATE item SET quant = :quant, "
                . "WHERE codProduto= :codProduto AND num = :num AND codPedido=:codPedido";
        try {
            $stmt = $this->c->prepare($sql);
            $stmt->bindValue(":quant", $item->getQuant());
            $stmt->bindValue(":num", $item->getNum());
            $stmt->bindValue(":codProduto", $item->getProduto()->getCod());
            $stmt->bindValue(":codPedido", $item->getPedido()->getCod());
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $exc) {
            throw new \Exception($exc->getMessage());
        }
    }

    public function excluir($num, $codProduto, $codPedido) {
        $sql = "DELETE FROM item WHERE codProduto= :codProduto AND num = :num AND codPedido=:codPedido";
        try {
            $stmt = $this->c->prepare($sql);
            $stmt->bindParam(":num", $num);
            $stmt->bindParam(":codProduto", $codProduto);
            $stmt->bindParam(":codPedido", $codPedido);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function buscarPorChave($num, $codProduto, $codPedido) {
        $sql = "SELECT * FROM item WHERE num = $num AND codProduto=$codProduto AND codPedido=$codPedido";
        try {
            $query = $this->c->prepare($sql);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Item");
            $rs = $query->fetchAll();
            if (count($rs) > 0) {
                return $rs[0];
            } else {
                return null;
            }
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function buscarPorCodPedido($cod) {
        $sql = "SELECT * FROM item WHERE codPedido = :cod";
        try {
            $query = $this->c->prepare($sql);
            $query->bindParam(":cod", $cod, PDO::PARAM_INT);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Item");
            $rs = $query->fetchAll();
            if (count($rs) > 0) {
                return $rs;
            } else {
                return null;
            }
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function buscarPorCodProduto($cod) {
        $sql = "SELECT * FROM item WHERE codProduto = :cod";
        try {
            $query = $this->c->prepare($sql);
            $query->bindParam(":cod", $cod, PDO::PARAM_INT);
            $query->execute();
            $query->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Item");
            $rs = $query->fetchAll();
            if (count($rs) > 0) {
                return $rs;
            } else {
                return null;
            }
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

}
