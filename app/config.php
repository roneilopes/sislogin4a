<?php

define("S", DIRECTORY_SEPARATOR);

function __autoload($classe){
    $pastas = 
    array("model", "controller", "view", "view".S."manipulaHtml","dao",
           "core", "helper");
    
    foreach ($pastas as $p){
        if(file_exists(__DIR__.S.$p.S.$classe.".php")){
            require_once __DIR__.S.$p.S.$classe.".php";
        }
    }
}