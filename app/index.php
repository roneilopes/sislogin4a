<?php
require 'config.php';

if(!isset($_GET['url'])){
    header("Location: /SisLogin4A/index.php");
}
$url = $_GET['url'];
$vetorUrl = explode("/", $url);
$control = $vetorUrl[0];
$metodo = $vetorUrl[1];
$parametros = array_slice($vetorUrl, 2);
$obj = new $control;

call_user_func_array(array($obj, $metodo), $parametros);