<?php

class Conexao {
    /**
     * @var PDO 
     */
    private static $conn;
    
    public static function getConn(){
        if(!self::$conn){
            $dsn = "mysql:host=localhost; dbname=sislogin4a; charset=utf8";
            try{
                self::$conn = new PDO($dsn,"root","");
                self::$conn->setAttribute(PDO::ATTR_ERRMODE,
                                          PDO::ERRMODE_EXCEPTION);
            } catch (Exception $ex) {
                return "ERRO:". $ex->getMessage();
            }
        }
        return self::$conn;
    }
}
