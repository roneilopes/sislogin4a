<?php
class Login {
    /**
     * @var UsuarioDao 
     */
    private $dao;
    
    public function __construct() {
        $this->dao = new UsuarioDao();
    }

    public function fazLogin($login, $senha){
        $u = $this->dao->buscarPorLoginSenha($login, $senha);
        if(is_object($u)){
            session_start();
            $_SESSION['usuario'] = $u->getNome();
            $_SESSION['login'] = $u->getLogin();
            $_SESSION['logado'] = true;
            return true;
        }
        return false;
    }
    public function fazLogout() {
        session_start();
        if(isset($_SESSION['logado']) && $_SESSION['logado']){
            session_destroy();
            unset($_SESSION);
            return true;
        }
        return false;
    }
    
}
