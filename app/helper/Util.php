<?php
class Util {
    
    public static function verMetodoEnvio($vetor){
        if($vetor == "POST"){
            $tipo = INPUT_POST;
        }else{
            $tipo = INPUT_GET;
        }
        return $tipo; 
    }
    
    public static function verificarLogin() {
        session_start();
        if(!isset($_SESSION['logado'])){
            header("Location: /SisLogin4A/app/LoginCtrl/verificarLogin");
        }
    }
}
