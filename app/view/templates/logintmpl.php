<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link href="/SisLogin4A/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span10">
                    {FORM_LOGIN}
                    {MENU}
                </div>
                <div class="span2">
                    {SAIR}
                </div>
            </div>    
        </div>
    </body>
</html>
