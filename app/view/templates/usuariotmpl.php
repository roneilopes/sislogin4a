<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{ACAO} usuários</title>
        <script src="/SisLogin4A/javascript/jquery.min.js"
            type="text/javascript"></script>
        <script src="/SisLogin4A/javascript/funcoes.js"
            type="text/javascript"></script>
        <link href="/SisLogin4A/css/bootstrap.css"
            rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid">
            <header class="span10 nav-header">
                <a href="/SisLogin4A/app/UsuarioCtrl/mostrarMenuUsuarios"
                   class="btn btn-link">
                       voltar ao menu
                </a>
                <a href="/SisLogin4A/" class="btn btn-link">
                       voltar a página inicial
                </a>
                <div class="span2">{BOTAOSAIR}</div>
            </header>
            <div class="row-fluid">
                <div class="span12">
                    {CONTEUDO}
                </div>
                <div id="pesq" class="span12">
                </div>
            </div>
        </div>
    </body>
</html>
