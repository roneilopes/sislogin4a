<?php

class UsuarioView {
    
    private $tempUsuario;
    
    public function __construct() {
        $this->tempUsuario = new TesteTemplate("view/templates/usuariotmpl.php");
        $this->btSair();
    }
    
    private function mostraTemplate($conteudo, $acao){
        $this->tempUsuario->CONTEUDO = $conteudo;
        $this->tempUsuario->ACAO = $acao;
        $this->tempUsuario->show();
    }

    public function btSair() {
        $loginView = new LoginView();
        $this->tempUsuario->BOTAOSAIR = $loginView->montarBotaoSair();
    }
    
    public function gerarOpcoesPesquisa() {
        $formOp = new Formulario("", "post", "form form-search");
        $radioCod = new Input("radio", "opcao", "Codigo: ","cod", "", "radio");
        $radioNome = new Input("radio", "opcao", "Nome: ","nome", "", "radio");
        $radioLogin = new Input("radio", "opcao", "Login: ","login", "", "radio");
        $field = new FieldSet("fieldset", "Escolha uma opção de pesquisa", "navbar");
        $field->addInputComLabelDepois(array($radioCod, $radioNome, $radioLogin));
        $formOp->setConteudo($field);
        $this->mostraTemplate($formOp->getTag(), "pesquisar");
    }
    
    public function montarMenu() {
        $divMenu = new TagComFechamento("div");
        $btNovo = new Link("../UsuarioCtrl/mostrarFormCadastro", "btn btn-large");
        $btNovo->setConteudo("Cadastrar Usuario");
        $btPesquisarUs = new Link("../UsuarioCtrl/mostrarOpcoesPesq", "btn btn-large");
        $btPesquisarUs->setConteudo("Pesquisar Usuarios");
        $divMenu->setConteudo(array($btNovo, $btPesquisarUs));
        $this->mostraTemplate($divMenu->getTag(), "menu");
    }
    
    public function formCadastro() {
        $formulario = $this->montarFormUsuario("salvarUsuario");
        $this->mostraTemplate($formulario, "salvar");
    }
    
    public function formAtualizar($cod, $nome, $login) {
        $formulario = $this->montarFormUsuario("atualizarUsuario",$cod, $nome, $login);
        $this->mostraTemplate($formulario, "atualizar");
    }
    
    public function formExcluir($cod, $nome, $login) {
        $formulario = $this->montarFormUsuario("excluirUsuario", $cod, $nome, $login);
        $this->mostraTemplate($formulario, "excluir");
    }
    
    public function montarFormUsuario($acao, $cod = "", $nome = "", $login = "") {
        $form = new Formulario("/SisLogin4A/app/UsuarioCtrl/". $acao."/".$cod, "post", "form-actions");
        $inpCod = new Input("text", "cod", "Código: ", $cod, "", "input-small", "cod");
        $inpCod->setAtributos("readOnly", "readOnly");
        $inpNome = new Input("text", "nome", "Nome: ", $nome, "Informe o nome do usuario", "input-xlarge", "nome");
        $inpLogin = new Input("text", "login", "Login: ", $login, "Informe o login", "input-xlarge", "login");
        return $this->montarConformeAcao($acao, $form, $inpCod, $inpNome, $inpLogin);
        
    }

    public function montarConformeAcao($acao, $form, $inpCod, $inpNome, $inpLogin) {
        $fieldCont = new FieldSet();
        $fieldCont->addInputComLabelAntes(array($inpCod, $inpNome, $inpLogin));
        if($acao != "excluirUsuario"){
            switch ($acao){
                case "salvarUsuario":
                    $rot1 = "Senha: ";
                    $name1 = "senha";
                    $rot2 = "Repetir Senha: ";
                    $name2 = "senha2";
                    break;
                case "atualizarUsuario":
                    $rot1 = "Senha anterior: ";
                    $name1 = "senhaAnt";
                    $rot2 = "Senha atual: ";
                    $name2 = "senhaAtual";
                    break;
            }
            $inpSenhaAnt = new Input("password", $name1, $rot1, "", "", "input-medium", $name1);
            $inpSenhaAtual = new Input("password", $name2, $rot1, "", "", "input-medium", $name2);
            $fieldCont->addInputComLabelAntes(array($inpSenhaAnt, $inpSenhaAtual));
        }
        $form->setConteudo($fieldCont);
        $acao =  split('r', $acao);
        $inpSub = new Input("submit", "", "", ucfirst($acao[0])."r", "", "btn btn-primary");
        $fieldBotoes = new FieldSet();
        if($acao[0] != "exclui"){
            $inpRes = new Input("reset", "", "", "Limpar", "", "btn btn-primary");
            $fieldBotoes->setConteudo(array($inpSub, $inpRes));
        }else{
            $fieldBotoes->setConteudo(array($inpSub));            
        }
        $form->setConteudo(array($fieldCont, $fieldBotoes));
        return $form->getTag();
    }
    
    public function montarFormPesqUsuario($tipo) {
        $form = new Formulario("/SisLogin4A/app/UsuarioCtrl/pesquisarUsuario/".$tipo,"post","form-actions");
        switch ($tipo) {
            case "cod":
                $name = "cod";
                $rotulo = "Código: ";
                break;
            case "nome":
                $name = "nome";
                $rotulo = "Nome: ";
                break;
            case "login":
                $name = "login";
                $rotulo = "Login: ";
                break;
        }
        $input = new Input("text", $name, $rotulo, "", "Informe o ".$name);
        $botao = new Input("submit", "", "", "Pesquisar", "", "btn");
        $field = new FieldSet("fieldset", "Pesquisar por ".$name);
        $field->addInputComLabelAntes($input);
        $field->setConteudo($botao);
        $form->setConteudo($field);
        echo $form->getTag();
    }
    
    public function montarTabelaUsuarios($usuarios) {
        $tituloCol = array("Codigo", "Nome", "Login", "Atualizar", "Excluir");
        $tabela = new Tabela($tituloCol, "table table-striped");
        foreach ($usuarios as $u){
            $btAtualizar = new Link("/SisLogin4A/app/UsuarioCtrl/mostrarFormAtualizar/{$u->getCod()}/atualizar", "btn btn-success");
            $btAtualizar->setConteudo("atualizar");
            $btExcluir = new Link("/SisLogin4A/app/UsuarioCtrl/mostrarFormAtualizar/{$u->getCod()}/excluir", "btn btn-danger");
            $btExcluir->setConteudo("excluir");
            $tabela->addLinhaCorpo(array( $u->getCod(), $u->getNome(),
                                          $u->getLogin(), $btAtualizar,
                                          $btExcluir));
        }
        $this->mostraTemplate($tabela->getTag(), "Resultado pesquisa");
    }
}