<?php

class ProdutoView {
    private $tempProduto;
    
    public function __construct() {
        $this->tempProduto = new TesteTemplate("view/templates/produtotmpl.php");
        $this->btSair();
    }
    
    private function mostraTemplate($conteudo, $acao){
        $this->tempProduto->CONTEUDO = $conteudo;
        $this->tempProduto->ACAO = $acao;
        $this->tempProduto->show();
    }

    public function btSair() {
        $loginView = new LoginView();
        $this->tempProduto->BOTAOSAIR = $loginView->montarBotaoSair();
    }
    
    public function gerarOpcoesPesquisa() {
        $formOp = new Formulario("", "post", "form form-search");
        $radioCod = new Input("radio", "opcao", "Codigo: ","cod", "", "radio");
        $radioDescricao = new Input("radio", "opcao", "Descrição: ","descricao", "", "radio");
        $field = new FieldSet("fieldset", "Escolha uma opção de pesquisa", "navbar");
        $field->addInputComLabelDepois(array($radioCod, $radioDescricao));
        $formOp->setConteudo($field);
        $this->mostraTemplate($formOp->getTag(), "pesquisar");
    }
    
    public function montarMenu() {
        $divMenu = new TagComFechamento("div");
        $btNovo = new Link("../ProdutoCtrl/mostrarFormCadastro", "btn btn-large");
        $btNovo->setConteudo("Cadastrar Produto");
        $btPesquisarUs = new Link("../ProdutoCtrl/mostrarOpcoesPesq", "btn btn-large");
        $btPesquisarUs->setConteudo("Pesquisar Produtos");
        $divMenu->setConteudo(array($btNovo, $btPesquisarUs));
        $this->mostraTemplate($divMenu->getTag(), "menu");
    }
    
    public function formCadastro() {
        $formulario = $this->montarFormProduto("salvarProduto");
        $this->mostraTemplate($formulario, "salvar");
    }
    
    public function formAtualizar($cod, $descricao, $valor, $marca) {
        $formulario = $this->montarFormProduto("atualizarProduto",$cod, $descricao, $valor, $marca);
        $this->mostraTemplate($formulario, "atualizar");
    }
    
    public function formExcluir($cod, $descricao, $valor, $marca) {
        $formulario = $this->montarFormProduto("excluirProduto", $cod, $descricao, $valor, $marca);
        $this->mostraTemplate($formulario, "excluir");
    }
    
    public function montarFormProduto($acao, $cod = "", $descricao = "", $valor = "", $marca = "") {
        $form = new Formulario("/SisLogin4A/app/ProdutoCtrl/". $acao."/".$cod, "post", "form-actions");
        $inpCod = new Input("text", "cod", "Código: ", $cod, "", "input-small", "cod");
        $inpCod->setAtributos("readOnly", "readOnly");
        $inpDescricao = new Input("text", "descricao", "Descricao: ", $descricao, "Informe a Descrição do Produto", "input-xlarge", "descricao");
        $inpValor = new Input("text", "valor", "Valor: ", $valor, "Informe o Valor", "input-xlarge", "valor");
        $inpMarca = new Input("text", "marca", "Marca: ", $marca, "Informe a Marca", "input-xlarge", "marca");
        return $this->montarConformeAcao($acao, $form, $inpCod, $inpDescricao, $inpValor, $inpMarca );
        
    }

    public function montarConformeAcao($acao, $form, $inpCod, $inpDescricao, $inpValor, $inpMarca) {
        $fieldCont = new FieldSet();
        $fieldCont->addInputComLabelAntes(array($inpCod, $inpDescricao, $inpValor, $inpMarca));
        $form->setConteudo($fieldCont);
        $acao =  split('r', $acao);
        $inpSub = new Input("submit", "", "", ucfirst($acao[0])."r", "", "btn btn-primary");
        $fieldBotoes = new FieldSet();
        if($acao[0] != "exclui"){
            $inpRes = new Input("reset", "", "", "Limpar", "", "btn btn-primary");
            $fieldBotoes->setConteudo(array($inpSub, $inpRes));
        }else{
            $fieldBotoes->setConteudo(array($inpSub));            
        }
        $form->setConteudo(array($fieldCont, $fieldBotoes));
        return $form->getTag();
    }
    
    public function montarFormPesqProduto($tipo) {
        $form = new Formulario("/SisLogin4A/app/ProdutoCtrl/pesquisarProduto/".$tipo,"post","form-actions");
        switch ($tipo) {
            case "cod":
                $name = "cod";
                $rotulo = "Código: ";
                break;
            case "descricao":
                $name = "descricao";
                $rotulo = "Descrição: ";
                break;
        }
        $input = new Input("text", $name, $rotulo, "", "Informe o ".$name);
        $botao = new Input("submit", "", "", "Pesquisar", "", "btn");
        $field = new FieldSet("fieldset", "Pesquisar por ".$name);
        $field->addInputComLabelAntes($input);
        $field->setConteudo($botao);
        $form->setConteudo($field);
        echo $form->getTag();
    }
    
    public function montarTabelaProdutos($produtos) {
        $tituloCol = array("Codigo", "Descrição", "Valor", "Marca", "Atualizar", "Excluir");
        $tabela = new Tabela($tituloCol, "table table-striped");
        foreach ($produtos as $p){
            $btAtualizar = new Link("/SisLogin4A/app/ProdutoCtrl/mostrarFormAtualizar/{$p->getCod()}/atualizar", "btn btn-success");
            $btAtualizar->setConteudo("atualizar");
            $btExcluir = new Link("/SisLogin4A/app/ProdutoCtrl/mostrarFormAtualizar/{$p->getCod()}/excluir", "btn btn-danger");
            $btExcluir->setConteudo("excluir");
            $tabela->addLinhaCorpo(array( $p->getCod(), $p->getDescricao(),
                                          $p->getValor(),$p->getMarca(), $btAtualizar,
                                          $btExcluir));
        }
        $this->mostraTemplate($tabela->getTag(), "Resultado pesquisa");
    }
}
