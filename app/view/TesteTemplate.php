<?php

class TesteTemplate {

    private $nomeArquivo;
    private $strArquivo;
    private $variaveis = array(); //nomes das variáveis
    private $valores = array(); //valores das variáveis
    //private $blocos = array(); //blocos existentes
    //private $blocosMostrar = array(); //blocos a serem mostrados
    private static $REG_NAME = "([[:alnum:]]|_)+";

    public function __construct($arquivo) {
        $this->nomeArquivo = $arquivo;
        $this->identVariaveis();
    }

    public function __get($var) {
        if (isset($this->valores["{" . $var . "}"])) {
            return $this->valores["{" . $var . "}"];
        } else {
            throw new \RuntimeException("var $var não existe");
        }
    }

    public function __set($var, $value) {
        if (!$this->exists($var)) {
            throw new \RuntimeException("var $var não existe");
        } else {
            $this->setValue($var, $value);
        }
    }

    //método para verificar se a variável existe
    public function exists($var) {
        return in_array("{" . $var . "}", $this->variaveis);
    }

    //método para definir o valor da variável
    private function setValue($var, $valor) {
        $this->valores['{' . $var . "}"] = $valor;
    }

    private function identVariaveis() {
        try {
            if (!file_exists($this->nomeArquivo) || !$this->isPhp()) {
                throw new \InvalidArgumentException('Arquivo inválido');
            } else {
                ob_start();
                require $this->nomeArquivo;
                $str = ob_get_contents();
                ob_end_clean();
                $this->strArquivo = $str;
                $this->montaVetorVariaveis($str);
                //$this->montaVetorBlocos($str);
            }
        } catch (Exception $ex) {
            throw new \InvalidArgumentException('Arquivo inválido' . $ex->getMessage());
        }
    }

    private function isPhp() {
        if (0 === strcasecmp(".php", substr($this->nomeArquivo, stripos($this->nomeArquivo, ".php")))) {
            return true;
        }
        return false;
    }
/*    
    public function mostrarBloco($nomeBloco){
        $this->blocosMostrar[$nomeBloco] = true;
    }
*/
    private function montaVetorVariaveis($str) {
        if (!empty($str)) {
            $ident = preg_match_all("/{(" . self::$REG_NAME . ")((\-\>(" . self::$REG_NAME . "))*)?((\|.*?)*)?}/", $str, $v);
            $this->variaveis = $v[0];
        }
    }

    private function substituiVariaveis() {
        foreach ($this->variaveis as $v) {
            if (isset($this->valores[$v])) {
                $this->strArquivo = str_replace($v, $this->valores[$v], $this->strArquivo);
            } else {
                $this->strArquivo = str_replace($v, "", $this->strArquivo);
            }
        }
    }
    /*
    private function montaVetorBlocos($arq) {
        if (!empty($arq)) {
            $res = preg_match_all("/<!-- BEGIN (.*?) -->/", $arq, $v);
            if ($res > 0) {
                $this->pegarRemoverConteudoBlocos($v);
                
            }
        }
    }
     * método que identifica os blocos e guarda seu conteúdo
     * em um vetor e retira o conteúdo da página.
     */
    /*
    private function pegarRemoverConteudoBlocos($v) {
        foreach ($v[1] as $block) {
            $localInicio = "BEGIN $block";
            $localFim = "END $block";
            //pegando a posição do início do bloco, o +4 é para passar do " -->"
            $inicio = stripos($this->strArquivo, $localInicio) + strlen($localInicio) + 4;
            //pegando a posição do fim do bloco, o -5 é para parar antes do "<!-- "
            $final = stripos($this->strArquivo, $localFim) - 5;
            $conteudo = substr($this->strArquivo, $inicio, $final - $inicio);
            $this->blocos[$block] = $conteudo;
            $this->strArquivo = str_replace($conteudo,"",$this->strArquivo);
        }
    }    
    public function adicionarNoBloco($bloco, $str) {
        if(!$this->existsBlock($bloco)){
            throw new \InvalidArgumentException('Bloco inválido' . $ex->getMessage());
        }else{
            $this->blocos[$bloco] += "\n".$str;
        }
    }
*/    
    public function show() {
        //$this->mostrarBlocos();
        $this->substituiVariaveis();
        echo $this->strArquivo;
    }
    /**
     * Método que percorre o vetor de blocos
     * encontra a marcação de início do bloco e insere o
     * conteúdo entre a marcação de início e de fim 
     * deste bloco.
     
    private function mostrarBlocos() {
        foreach ($this->blocos as $nomeBloco => $b) {
            if(isset($this->blocosMostrar[trim($nomeBloco)])){
                $this->insereConteudoBloco($b);
            }
        }
    }

    private function insereConteudoBloco($b) {
        $tam = strlen($this->strArquivo);
        $localInicio = "BEGIN $b";
        //pegando a posição do início do bloco, o +4 é para passar do " -->"
        $inicio = stripos($this->strArquivo, $localInicio) + strlen($localInicio) + 4;
        $arqInicio = substr($this->strArquivo, 0,($inicio - $tam));
        $arqFim = substr($this->strArquivo, $inicio);
        $this->strArquivo = $arqInicio . $b . $arqFim;
        
    }
    */
}
