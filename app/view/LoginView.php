<?php

class LoginView {
    public function montarFormLogin() {
        $form = new Formulario("../LoginCtrl/logar", "post", "form-actions");
        $inpLogin = new Input("text", "login", "Login: ","", "Informe seu login", "input-xlarge", "login");
        $inpSenha = new Input("password", "senha", "Senha: ","", "", "input-medium", "senha");
        $inpSub = new Input("submit", "", "", "Entrar", "", "btn btn-primary");
        $inpRes = new Input("reset", "", "", "Limpar", "", "btn btn-primary");
        $fieldCont = new FieldSet();
        $fieldCont->addInputComLabelAntes($inpLogin);
        $fieldCont->addInputComLabelAntes($inpSenha);
        $fieldBotoes = new FieldSet();
        $fieldBotoes->setConteudo(array($inpSub, $inpRes));
        $form->setConteudo(array($fieldCont, $fieldBotoes));
        return $form->getTag();
    }
    
    public function montarMenu() {
        $divMenu = new TagComFechamento("div");
        $btProdutos = new Link("../ProdutoCtrl/mostrarMenuProdutos", "btn btn-large");
        $btProdutos->setConteudo("Produtos");
        $btPedidos = new Link("../PedidoCtrl/mostrarMenuPedidos", "btn btn-large");
        $btPedidos->setConteudo("Pedidos");
        $btUsuarios = new Link("../UsuarioCtrl/mostrarMenuUsuarios", "btn btn-large");
        $btUsuarios->setConteudo("Manter Usuarios");
        $divMenu->setConteudo(array($btProdutos,$btPedidos,$btUsuarios));
        return $divMenu->getTag();
    }
    
    public function montarBotaoSair() {
        $botaoSair = new TagComFechamento("a", "btn btn-danger");
        $botaoSair->setAtributos("href", "../LoginCtrl/sair");
        $botaoSair->setConteudo("Sair");
        return $botaoSair->getTag();
    }
}
