<?php
/**
 * Cria uma Tag genérica
 *
 * @author Faino
 */
class TagSemFechamento extends Tag{
    
    public function __construct($tag, $classe = "", $id = "", $name = "") {
        parent::__construct($tag, $classe, $id, $name);
    }
    
    public function finalizaTag(){
        $html = "/>";
        return $html;
    }
}
