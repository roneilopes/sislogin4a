<?php
class Input extends TagSemFechamento{
    private $label;
    private $type;
    private $value;
    
    public function __construct($type,$name, $rotulo="",$value="", $placeHolder="", $classe="",$id="") {
        parent::__construct("input",$classe,$id,$name);
        $this->montaInput($type,$rotulo,$value,$placeHolder);
    }
    
    private function montaInput($type,$rotulo, $value, $placeHolder){
        if($rotulo!== ""){
            $this->criarLabel($this->name,$rotulo);
        }
        $this->setAtributos("type", $type);
        $this->setAtributos("value", $value);
        $this->setAtributos("placeholder", $placeHolder);
    }

    private function criarLabel($id,$rotulo){
        $this->label = new TagComFechamento("label");
        $this->label->setAtributos("for", $id);
        $this->label->setConteudo($rotulo);
    }
    
    /**
     * @return TagComFechamento
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * @return Atributo
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return Atributo
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @param TagComFechamento $label
     */
    public function setLabel($label) {
        $this->label = $label;
    }

    /**
     * @param Atributo $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @param Atributo $value
     */
    public function setValue($value) {
        $this->value = $value;
    }
    
}
