<?php

class Atributo {
    private $desc;
    private $valor;
    
    public function __construct($desc='',$valor=''){
        $this->desc = $desc;
        $this->valor = $valor;
    }
    function getDesc() {
        return $this->desc;
    }

    function getValor() {
        return $this->valor;
    }

    function setDesc($desc) {
        $this->desc = $desc;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }


    
}
