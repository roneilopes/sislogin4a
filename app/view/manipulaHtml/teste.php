<?php
require "../../config.php";

$formulario = new Formulario("#", "post");

$fieldSet = new FieldSet("Dados Gerais");
$inputNome = new Input("text", "nome", "Nome:", "","informe seu nome");
$inputIdade = new Input("number", "idade", "Idade:", "", "idade");
$inputSubmit = new Input("submit", "enviar", "", "Enviar");
$selec = new Select("hobbies","hobbies","Hobbies:","Por favor escolha...");
$selec->setOpcao("Esportes", "ES");
$selec->setOpcao("Games", "GM");
$selec->setOpcao("Leitura", "LE");

$fieldSet->setConteudo($inputNome->getLabel());
$fieldSet->setConteudo($inputNome);
$fieldSet->setConteudo($inputIdade->getLabel());
$fieldSet->setConteudo($inputIdade);
$fieldSet->setConteudo($selec->getLabel());
$fieldSet->setConteudo($selec);
$fieldSet->setConteudo($inputSubmit);


$formulario->setConteudo($fieldSet);

echo $formulario->getTag();