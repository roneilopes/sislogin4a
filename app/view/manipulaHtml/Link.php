<?php
class Link extends TagComFechamento{
    
    public function __construct($href="", $classe = "", $tag="a", $id = "", $name = "") {
        parent::__construct($tag, $classe, $id, $name);
        $this->setAtributos("href", $href);
    }
    
    
}
