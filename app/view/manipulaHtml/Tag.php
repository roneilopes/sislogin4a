<?php
/**
 * Cria uma Tag genérica
 *
 * @author Faino
 */
abstract class Tag {
    protected $tag;
    protected $atributos=[];
    protected $classe;
    protected $id;
    protected $name;
    
    public function __construct($tag,$classe="",$id="",$name="") {
        $this->tag = $tag;
        $this->id = $id;
        $this->classe = $classe;
        $this->name = $name;
        $this->setAtributos("class", $classe);
        $this->setAtributos("id", $id);
        $this->setAtributos("name", $name);
    }

    public function iniciaTag(){
        $html = "\t<{$this->tag} ";
        return $html;
    }
    
    public function setAtributos($nomeAtributo, $valorAtributo=""){
        if($valorAtributo !== ""){
            $this->atributos[] = new Atributo($nomeAtributo, $valorAtributo);
        }
    }
    
    protected abstract function finalizaTag();

    protected function getNomeTag(){
        return $this->tag;
    }
    
    protected function defineAtributos(){
        $strAtributos="";
        foreach ($this->atributos as $atr){
           $strAtributos .= $atr->getDesc() . "='" . $atr->getValor()."' ";
        }
        return $strAtributos;
    } 
    
    public function getTag(){
        $html = $this->iniciaTag() . $this->defineAtributos().$this->finalizaTag()."\n";
        return $html;
    }
    
    protected function getAtributos(){
        return $this->atributos;
    }
    
    public function setTag($tag){
        $this->tag = $tag;
    }
}
