<?php

/**
 * Descrição de tags com fechamento
 * ex: <p></p> <a></a> etc
 *
 * @author Faino
 */
class TagComFechamento extends Tag{
    private $conteudo=[];
    
    public function __construct($tag, $classe = "", $id = "", $name = "") {
        parent::__construct($tag, $classe, $id, $name);
    }
    
    public function getConteudo() {
        return $this->conteudo;
    }
    /**
     * recebe um conteúdo ou um array de conteudos para a tag
     * @param mixed $conteudo
     */
    public function setConteudo($conteudo) {
        if(!is_null($conteudo)){
            if(is_array($conteudo)){
                $this->conteudo = $conteudo;
            }else{
                $this->conteudo[] = $conteudo;
            }
        }else{
            $this->conteudo[] = "";
        }
    }
    
    public function getTag() {
        $html = "\n";
        $html .= $this->iniciaTag();
        foreach ($this->conteudo as $conteudo) {
            $html .= "\t";
            if(is_string($conteudo)){
                $html .= $conteudo;
            }else{
                $html .= $conteudo->getTag();
            }
        }
        $html .= $this->finalizaTag();
        return $html;
    }

    public function iniciaTag() {
        return parent::iniciaTag().parent::defineAtributos().">";
    }
    
    public function finalizaTag() {
        $html = "</".parent::getNomeTag().">\n\t";
        return $html;
    }
}
