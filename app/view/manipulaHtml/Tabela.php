<?php
class Tabela extends TagComFechamento{
    private $thead;
    private $tbody;
    private $tfoot;

    public function __construct($titulosColunas,$classe="",$id="",$name=""){
        parent::__construct("table",$classe,$id,$name);
        $this->definirEstruturaTabela();        
        $this->criarRotulos($titulosColunas);
    }
    
    private function definirEstruturaTabela(){
        $this->thead = new TagComFechamento("thead");
        $this->tbody = new TagComFechamento("tbody");
        $this->tfoot = new TagComFechamento("tfoot");
        $this->setConteudo($this->thead);
        $this->setConteudo($this->tbody);
        $this->setConteudo($this->tfoot);
    }
    
    private function criarRotulos($titulosColunas){
        $linha = new TagComFechamento("tr");
        foreach ($titulosColunas as $coluna){
            $col = new TagComFechamento("th");
            $col->setConteudo($coluna);
            $linha->setConteudo($col);
        }
        $this->thead->setConteudo($linha); //mudei de lugar.
    }
    /**
     * @param array $conteudoColunas 
     * um vetor com os conteudos das colunas
     */
    public function addLinhaCorpo($conteudoColunas){
        $linha = new TagComFechamento("tr");
        foreach ($conteudoColunas as $conteudo){
            $col = new TagComFechamento("td");
            $col->setConteudo($conteudo);
            $linha->setConteudo($col);
        }
        $this->tbody->setConteudo($linha);
    }
    
    /**
     * @param array $conteudoColunas - um vetor com os conteudos das colunas
     */
    public function addLinhaRodape($conteudoColunas){
        foreach ($conteudoColunas as $conteudo){
            $linha = new TagComFechamento("tr");
            $col = new TagComFechamento("td");
            $col->setConteudo($conteudo);
            $linha->setConteudo($col);
            $this->tfoot->setConteudo($linha);
        }
    }
    
    public function getThead() {
        return $this->thead;
    }

    public function getTbody() {
        return $this->tbody;
    }

    public function getTfoot() {
        return $this->tfoot;
    }

}
