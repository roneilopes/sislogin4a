<?php

class FieldSet extends TagComFechamento{
    private $legend;

    public function __construct($tag="fieldset", $legenda="" , $classe = "", $id = "", $name = "") {
        parent::__construct($tag, $classe, $id, $name);
        if($legenda !== ""){
            $this->setLegend($legenda);
        }
    }        
    public function setLegend($legenda){
        $this->legend = new TagComFechamento("legend");
        $this->legend->setConteudo($legenda);
        $this->setConteudo($this->legend);
    }
    
    public function getTextoLegend(){
        return $this->legend->getConteudo();
    }
    public function getLegend(){
        return $this->legend;
    }

    /**
     * @param array<Input> $inputs
     */
    public function addInputComLabelAntes($inputs){
        if (is_array($inputs)){
            foreach ($inputs as $input){
                $this->setConteudo($input->getLabel());
                $this->setConteudo($input);                
            }
        }else{
            $this->setConteudo($inputs->getLabel());
            $this->setConteudo($inputs);
        }
    }

    /**
     * @param array<Input> $inputs
     */
    public function addInputComLabelDepois($inputs){
        if (is_array($inputs)){
            foreach ($inputs as $input){
                $this->setConteudo($input);                
                $this->setConteudo($input->getLabel());
            }
        }else{
            $this->setConteudo($inputs);
            $this->setConteudo($inputs->getLabel());
        }
    }    


}
