<?php
class Formulario extends TagComFechamento{
    private $metodo;
    private $destino;
    
    public function __construct($destino,$metodo="",$classe = "", $id = "", $name = ""){
        parent::__construct("form",$classe,$id,$name);
        $this->setAtributos("action", $destino);
        $this->setAtributos("method", $metodo);
    }
    
    public function getMetodo() {
        return $this->metodo;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function setMetodo($metodo) {
        $this->metodo = $metodo;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }
    /**
     * @param Input $input
     */
    public function addInputComLabelAntes($input){
        $this->setConteudo($input->getLabel());
        $this->setConteudo($input);
    }
    /**
     * @param Input $input
     */
    public function addInputComLabelDepois($input){
        $this->setConteudo($input);
        $this->setConteudo($input->getLabel());
    }    
    
}
