<?php
class Select extends TagComFechamento{
    private $label;
    
    public function __construct($name,$id="",$rotulo="",$placeHolder="", $classe="",$multiple="",$required="") {
        parent::__construct("select",$classe,$id,$name);
        $this->iniciarSelect($rotulo,$placeHolder,$multiple,$required);
    }
    
    private function iniciarSelect($rotulo="",$placeHolder="",$multiple="",$required=""){
        if($rotulo!== ""){
            $this->criarLabel($this->id,$rotulo);
        }
        if($placeHolder !== ""){
            $this->definirPlaceHolder($placeHolder);
        }
        $this->setAtributos("multiple",$multiple);
        $this->setAtributos("required",$required);
    }
    
    private function criarLabel($id,$rotulo){
        $this->label = new TagComFechamento("label");
        $this->label->setAtributos("for", $id);
        $this->label->setConteudo($rotulo);
    }
    private function definirPlaceHolder($place){
        $op = new TagComFechamento("option");
        $op->setAtributos("value", "");
        $op->setAtributos("disabled","disabled");
        $op->setAtributos("selected","selected");
        $op->setAtributos("hidden","hidden");
        $op->setConteudo($place);
        $this->setConteudo($op);
    }
    public function getLabel(){
        return $this->label;
    }
    
    public function setOpcao($texto,$valor="",$marcado=false){
        $opcao = new TagComFechamento("option");
        $opcao->setAtributos("value", $valor);
        if($marcado){
            $opcao->setAtributos("selected", "selected");
        }
        $opcao->setConteudo($texto);
        $this->setConteudo($opcao);
    }
}
