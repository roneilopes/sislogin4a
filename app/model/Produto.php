<?php
class Produto {
    private $cod;
    private $descricao;
    private $marca;
    private $valor;
    
    public function __construct($cod="", $descricao="", $valor="", $marca="") {
        $this->cod = $cod;
        $this->descricao = $descricao;
        $this->marca = $marca;
        $this->valor = $valor;
    }

    public function getValor() {
        return $this->valor;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function getCod() {
        return $this->cod;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getMarca() {
        return $this->marca;
    }

    public function setCod($cod) {
        $this->cod = $cod;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setMarca($marca) {
        $this->marca = $marca;
    }


}
