<?php

class Pedido {
    private $cod;
    private $total;
    private $cliente;
    private $data;
    private $itens = array();
    
    public function __construct($cod=0, $total=0.0, 
                                $cliente="", $data="") {
        $this->cod = $cod;
        $this->total = $total;
        $this->cliente = $cliente;
        $this->data = $data;
    }

    
    public function getCod() {
        return $this->cod;
    }

    public function getTotal() {
        return $this->total;
    }

    public function getCliente() {
        return $this->cliente;
    }

    public function getData() {
        return $this->data;
    }

    public function getItens() {
        return $this->itens;
    }

    public function setCod($cod) {
        $this->cod = $cod;
    }

    public function setCliente($cliente) {
        $this->cliente = $cliente;
    }

    public function setData($data) {
        $this->data = $data;
    }
    /**
     * @param Item $item
     */
    public function addItens($item) {
        $this->total += $item->getValor();
        $this->itens[] = $item;
    }
    public function setItens($itens){
        foreach($itens as $i){
            $this->addItens($i);
        }
    }
    public function removeItem($num){
        foreach ($this->itens as $i){
            if($i->getNum() === $num){
                $this->total -= $i->getValor();
                unset($this->itens[$num-1]);
            }
        }
    }
    
}
