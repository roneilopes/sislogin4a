<?php
class Item {
    private $num;
    /**
     * @var Produto 
     */
    private $produto;
    /**
     * @var Pedido 
     */
    private $pedido;
    private $quant;
    
    public function __construct($num=0, $produto=null, 
                                $pedido=null,$quant=0) {
        $this->num = $num;
        $this->produto = $produto;
        $this->pedido = $pedido;
        $this->quant = $quant;
    }
    
    public function getValor() {
        return $this->produto->getValor() *
                       $this->quant;
    }
    
    public function getCodigo() {
        return $this->codigo;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

        public function getNum() {
        return $this->num;
    }


    public function getProduto() {
        return $this->produto;
    }

    public function getQuant() {
        return $this->quant;
    }

    public function setNum($num) {
        $this->num = $num;
    }

    public function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    public function setQuant($quant) {
        $this->quant = $quant;
    }    
    public function getPedido() {
        return $this->pedido;
    }

    public function setPedido(Pedido $pedido) {
        $this->pedido = $pedido;
    }

}
