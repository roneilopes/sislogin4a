<?php

class Usuario {
    private $cod;
    private $nome;
    private $login;
    private $senha;
    
    function __construct($cod="", $nome="", $login="", $senha="") {
        $this->cod = $cod;
        $this->nome = $nome;
        $this->login = $login;
        $this->senha = $senha;
    }
    
    public function getCod() {
        return $this->cod;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getLogin() {
        return $this->login;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function setCod($cod) {
        $this->cod = $cod;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }



}
