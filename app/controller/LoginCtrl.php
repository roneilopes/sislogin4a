<?php
class LoginCtrl {
    private $login;
    private $loginView;
    private $metodoEnvio;
    private $tempLogin;
    
    public function __construct() {
        $this->login = new Login();
        $this->tempLogin = new TesteTemplate("view/templates/logintmpl.php");
        $this->metodoEnvio = Util::verMetodoEnvio($_SERVER['REQUEST_METHOD']);
        $this->loginView = new LoginView();
    }
    
    public function logar() {
        $login = filter_input($this->metodoEnvio, "login");
        $senha = filter_input($this->metodoEnvio, "senha");
        $this->login->fazLogin($login,$senha);
        header("Location:../LoginCtrl/verificarLogin");
    }
    public function sair(){
        $this->login->fazLogout();
        header("Location:../LoginCtrl/verificarLogin");
    }
    
    private function mostrarFormLogin() {
        $this->tempLogin->FORM_LOGIN = $this->loginView->montarFormLogin();
        $this->tempLogin->show();
    }
    
    private function mostrarOpcoes() {
        $this->tempLogin->MENU = $this->loginView->montarMenu();
        $botaoSair = $this->loginView->montarBotaoSair();
        $this->tempLogin->SAIR = $botaoSair;
        $this->tempLogin->show();
    }
    
    public function verificarLogin() {
        session_start();
        if(isset($_SESSION['logado']) && $_SESSION['logado']){
            $this->mostrarOpcoes();
        }else{
            $this->mostrarFormLogin();
        }
    }
}