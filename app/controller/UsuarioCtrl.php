<?php

class UsuarioCtrl {
    private $metodoEnvio;
    private $usuarioView;
    
    public function __construct() {
        Util::verificarLogin();
        $this->metodoEnvio = Util::verMetodoEnvio($_SERVER['REQUEST_METHOD']);
        $this->usuarioView = new UsuarioView();
    }
    
    public function mostrarOpcoesPesq() {
        $this->usuarioView->gerarOpcoesPesquisa();
    }
    
    public function mostrarMenuUsuarios() {
        $this->usuarioView->montarMenu();
    }
    
    public function mostrarFormCadastro() {
        $this->usuarioView->formCadastro();
    }
    
    public static function verificarLogin() {
        session_start();
        if(!isset($_SESSION['logado'])){
            header("Location: /SisLogin4A/app/LoginCtrl/verificarLogin");
        }
    }
    
    public function mostrarFormPesq() {
        $tipo = filter_input($this->metodoEnvio, 'opcao');
        $this->usuarioView->montarFormPesqUsuario($tipo);
    }
    
    public function mostrarFormAtualizar($codUsuario, $acao) {
        $dao = new UsuarioDao();
        $us = $dao->buscarPorCod($codUsuario);
        $metodo = $acao == "atualizar" ? "formAtualizar" : "formExcluir";
        $this->usuarioView->$metodo($us->getCod(), $us->getNome(), $us->getLogin());
    }
    
    public function salvarUsuario() {
        $nome = filter_input($this->metodoEnvio, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $login = filter_input($this->metodoEnvio, "login");
        $senha = filter_input($this->metodoEnvio, "senha");
        $senhaAnt = filter_input($this->metodoEnvio, "senha2");
        if($senha === $senhaAnt){
            $this->concluirSalvar($nome, $login, $senha);
        }else{
            echo "<script> alert('Não foi possível salvar');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }

    public function concluirSalvar($nome, $login, $senha) {
        $u = new Usuario("", $nome, $login, $senha);
        $dao = new UsuarioDao();
        if($dao->salvar($u) == 1){
            echo "<script> alert('Usuario Cadastrado com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/UsuarioCtrl/mostrarMenuUsuarios');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível salvar');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }
    
    public function pesquisarUsuario($op) {
        $param = filter_input($this->metodoEnvio, $op);
        $dao = new UsuarioDao();
        if($param != ""){
            $metodo = "buscarPor".ucfirst($op);
            $result = $dao->$metodo($param);
        }else{
            $metodo = "buscarTodos";
            $result = $dao->$metodo();
        }
        if (is_array($result)){
            $this->usuarioView->montarTabelaUsuarios($result);
        }else{
            $this->usuarioView->montarTabelaUsuarios(array($result));
        }        
    }

    public function atualizarUsuario() {
        $cod = filter_input($this->metodoEnvio, "cod", FILTER_SANITIZE_NUMBER_INT);
        $nome = filter_input($this->metodoEnvio, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $login = filter_input($this->metodoEnvio, "login", FILTER_SANITIZE_STRING);
        $senhaAnt = filter_input($this->metodoEnvio, "senhaAnt");
        $senhaAtual = filter_input($this->metodoEnvio, "senhaAtual");
        $dao = new UsuarioDao();
        $u = $dao->buscarPorCod($cod);
        if($senhaAnt === $u->getSenha()){
            $this->concluirAtualizar($nome, $login, $senhaAtual, $u, $dao);
        }else{
            echo "<script> alert('Não foi possível atualizar');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }

    public function concluirAtualizar($nome, $login, $senhaAtual, $u, $dao) {
        $u->setNome($nome);
        $u->setLogin($login);
        $u->setSenha($senhaAtual);
        if($dao->atualizar($u) === 1){
            echo "<script> alert('Usuario Atualizado com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/UsuarioCtrl/mostrarMenuUsuarios');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível atualizar');"
            ."window.history.go(-1);"
            ."</script>";
        }        
    }
    
    public function excluirUsuario($cod) {
        $dao = new UsuarioDao();
        if ($dao->excluir($cod) == 1){
            echo "<script> alert('Usuario excluído com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/UsuarioCtrl/mostrarMenuUsuarios');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível excluir');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }

}