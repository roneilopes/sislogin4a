<?php

class ProdutoCtrl {
    
    private $metodoEnvio;
    private $produtoView;
    
    public function __construct() {
        Util::verificarLogin();
        $this->metodoEnvio = Util::verMetodoEnvio($_SERVER['REQUEST_METHOD']);
        $this->produtoView = new ProdutoView();
    }
    
    public function mostrarOpcoesPesq() {
        $this->produtoView->gerarOpcoesPesquisa();
    }
    
    public function mostrarMenuProdutos() {
        $this->produtoView->montarMenu();
    }
    
    public function mostrarFormCadastro() {
        $this->produtoView->formCadastro();
    }
    
    public static function verificarLogin() {
        session_start();
        if(!isset($_SESSION['logado'])){
            header("Location: /SisLogin4A/app/LoginCtrl/verificarLogin");
        }
    }
    
    public function mostrarFormPesq() {
        $tipo = filter_input($this->metodoEnvio, 'opcao');
        $this->produtoView->montarFormPesqProduto($tipo);
    }
    
    public function mostrarFormAtualizar($codProduto, $acao) {
        $dao = new ProdutoDao();
        $us = $dao->buscarPorCod($codProduto);
        $metodo = $acao == "atualizar" ? "formAtualizar" : "formExcluir";
        $this->produtoView->$metodo($us->getCod(), $us->getDescricao(), $us->getValor(), $us->getMarca());
    }
    
    public function salvarProduto() {
        $desc = filter_input($this->metodoEnvio, "descricao", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $valor = filter_input($this->metodoEnvio, "valor");
        $marca = filter_input($this->metodoEnvio, "marca");
        $this->concluirSalvar($desc, $valor, $marca);
    }

    public function concluirSalvar($desc, $valor, $marca) {
        $u = new Produto("", $desc, $valor, $marca);
        $dao = new ProdutoDao();
        if($dao->salvar($u) == 1){
            echo "<script> alert('Produto Cadastrado com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/ProdutoCtrl/mostrarMenuProdutos');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível salvar');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }
    
    public function pesquisarProduto($op) {
        $param = filter_input($this->metodoEnvio, $op);
        $dao = new ProdutoDao();
        if($param != ""){
            $metodo = "buscarPor".ucfirst($op);
            $result = $dao->$metodo($param);
        }else{
            $metodo = "buscarTodos";
            $result = $dao->$metodo();
        }
        if(is_array($result)){
            $this->produtoView->montarTabelaProdutos($result);
        }else{
            $this->produtoView->montarTabelaProdutos(array($result));
        }
        
    }

    public function atualizarProduto() {
        $cod = filter_input($this->metodoEnvio, "cod", FILTER_SANITIZE_NUMBER_INT);
        $desc = filter_input($this->metodoEnvio, "descricao", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $valor = filter_input($this->metodoEnvio, "valor");
        $marca = filter_input($this->metodoEnvio, "marca");
        $dao = new ProdutoDao();
        $u = $dao->buscarPorCod($cod);
        $this->concluirAtualizar($desc, $valor, $marca, $u, $dao);
    }

    public function concluirAtualizar($desc, $valor, $marca, $u, $dao) {
        $u->setDescricao($desc);
        $u->setValor($valor);
        $u->setMarca($marca);
        if($dao->atualizar($u) === 1){
            echo "<script> alert('Produto Atualizado com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/ProdutoCtrl/mostrarMenuProdutos');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível atualizar');"
            ."window.history.go(-1);"
            ."</script>";
        }        
    }
    
    public function excluirProduto($cod) {
        $dao = new ProdutoDao();
        if ($dao->excluir($cod) == 1){
            echo "<script> alert('Produto excluído com Sucesso!!!');"
            ."window.location.replace('/SisLogin4A/app/ProdutoCtrl/mostrarMenuProdutos');"
            ."</script>";
        }else{
            echo "<script> alert('Não foi possível excluir');"
            ."window.history.go(-1);"
            ."</script>";
        }
    }

}